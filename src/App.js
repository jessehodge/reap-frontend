import React from 'react';
import './App.css';
import {
  BrowserRouter as Router,

  Route,
} from "react-router-dom";
import LoginComponent from './components/LoginComponent';
import HomeComponent from './components/HomeComponent';
import RegisterComponent from './components/RegisterComponent';


function App() {
  return (
    <Router className="flex">
      <Route exact path='/' component={LoginComponent} />
      <Route exact path='/home' component={HomeComponent} />
      <Route exact path='/register' component={RegisterComponent} />
    </Router>
  );
}

export default App;
