import React, { Component } from "react";
import PhotoContainer from './PhotoContainer';

export default class HomeComponent extends Component {

    constructor(props) {
        super(props);

        this.state = { photos: [], image: null, image_description: '' }
    }

    componentDidMount() {
        fetch('http://localhost:5000/image/', {
            method: 'GET'
        }).then(response => {
            if (!response.ok) {
                throw Error("Error fetching images.");
            }
            return response.json()
        }).then(allData => {
            this.setState({photos: allData})
        }).catch(error => {
            throw Error(error.message)
        })
    }

    handleChange = event => {
        const { name, value } = event.target;
        this.setState({ [name]: value });
    }

    imageChange = event => {
        this.setState({
            image: event.target.files[0]
        });
    }

    handleSubmit = (event) => {
        event.preventDefault();
        const data = new FormData()
        data.append('image', this.state.image)
        data.append('image_description', this.state.image_description)
        data.append('user_id', localStorage.getItem('user_id'))
        fetch('http://localhost:5000/image/', {
            method: 'POST', body: data
        })
            .then(response => response.json())
            .then((data) => {
                if (data.status === 'success') {
                    alert("Your image was uploaded!");
                } else {
                    alert(data.message);
                }
            });
        window.location.reload();
    }

    handleClick = () => {
        localStorage.removeItem('Authorization');
        localStorage.removeItem('user_id');
        this.props.history.push('/');
    }

    render() {
        const { image_description } = this.state

        return (
            <div>
                <div>
                    <PhotoContainer photos={this.state.photos} />
                </div>

                <div>
                    <h2>Upload a file</h2>
                </div>
                <div>
                    <form onSubmit={this.handleSubmit} style={{border: "12px solid #000000"}}>
                        <label>Select a file to upload</label>
                        <input name='image' type='file' onChange={this.imageChange} />
                        <label>Image description</label>
                        <input name='image_description' type='text' onChange={this.handleChange} value={image_description} />

                        <input type="submit" value="Save Image" onClick={event => this.handleSubmit(event)} />
                    </form>
                    <button onClick={this.handleClick}>Logout</button>
                </div>
            </div>
        )
    }
}