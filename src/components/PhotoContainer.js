import React from 'react';
import Image from 'react-image-resizer';



const Photo = (props) => {
    return (
        <div className="container flex flex-row">
            <Image 
            src={props.url.image_url} 
            height={200}
            width={200}
            alt="doggo" />
            <div>{props.url.image_description}</div>
        </div>
    )
}

const PhotoContainer = props => {

    const displayPhotos = () => {
        if (props.photos.data) {
            return props.photos.data.map(photo => {
                return <Photo key={photo.created_at.toString()} url={photo} />
            });
        }
    };

    return (
        <section>{displayPhotos()}</section>
    )
}




export default PhotoContainer