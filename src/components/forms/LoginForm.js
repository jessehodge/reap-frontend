import React, { Component } from 'react';
import { Link, withRouter } from "react-router-dom";
import '../AuthComponent.css';

class LoginForm extends Component {

    constructor(props) {
        super(props);
        this.state = { email: '', password: '' }
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleSubmit(event) {
        event.preventDefault();
        const payload = {
            'password': this.state.password,
            'email': this.state.email
        }
        fetch('http://localhost:5000/auth/login', {
            method: 'POST', body: JSON.stringify(payload), headers: {
                'Content-Type': 'application/json',
            },
        })
            .then(response => response.json())
            .then(data => {
                if (data.status === 'success') {
                    localStorage.setItem('jwt', data['Authorization']);
                    localStorage.setItem('user_id', data['user_id']);
                    this.props.history.push('/home');
                } else {
                    alert(data.message);
                }
            })
    }

    handleChange = event => {
        const { name, value } = event.target;
        this.setState({ [name]: value });
    }

    render() {
        const { email, password } = this.state
        return (
        <div className="login-box">
        <h1>Login</h1>
            <form onSubmit={this.handleSubmit} className="flex">
            <label>Email</label>
                <div className="textbox">
                    <input name="email" type="text" onChange={this.handleChange} value={email} />
                </div>
                <label>Password</label>
                <div className="textbox">
                    <input name="password" type="password" onChange={this.handleChange} value={password} />
                </div>
                <input className="btn" type="submit" value="Login" onClick={event => this.handleSubmit(event)} />
            </form>
            <Link to={{ pathname: '/register' }}>Register</Link>
        </div>
        )
    }
}

export default withRouter(LoginForm);