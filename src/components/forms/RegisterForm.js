import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import { Link } from "react-router-dom";

import '../AuthComponent.css';

class RegisterForm extends Component {
    constructor(props) {
        super(props);
        this.state = { username: '', email: '', password: '' }

        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleSubmit(event) {
        event.preventDefault();
        const payload = {
            'username': this.state.username,
            'password': this.state.password,
            'email': this.state.email
        }
        fetch('http://localhost:5000/user/', {
            method: 'POST', body: JSON.stringify(payload), headers: {
                'Content-Type': 'application/json',
            },
        })
            .then(response => response.json())
            .then(data => {
                if (data.status === 'fail') {
                    alert(data.message)
                } else {
                    this.props.history.push('/home')
                }
            })
    }

    handleChange = event => {
        const { name, value } = event.target;
        this.setState({ [name]: value });
    }

    render() {
        const { email, password, username } = this.state
        return (
            <div className="login-box">
                <h1>Registration</h1>
                    <form onSubmit={this.handleSubmit} className="flex">
                        <label>Username</label>
                        <div className="textbox">
                            <input name="username" type="text" onChange={this.handleChange} value={username} />
                        </div>
                        <label>Email</label>
                        <div className="textbox">
                            <input name="email" type="text" onChange={this.handleChange} value={email} />
                        </div>
                        <label>Password</label>
                        <div className="textbox">
                            <input name="password" type="password" onChange={this.handleChange} value={password} />
                        </div>
                        <input className="btn" type="submit" value="Register" onClick={event => this.handleSubmit(event)} />
                    </form>
                    <Link to={{ pathname: '/' }}>Login</Link>
            </div>
        )
    }
}

export default withRouter(RegisterForm);