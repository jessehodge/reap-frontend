import React from "react"
import { Component } from "react";

import LoginForm from './forms/LoginForm';


export default class LoginComponent extends Component {
    render() {
        return (
            <div>
                <LoginForm />
            </div>
        )
    }
}